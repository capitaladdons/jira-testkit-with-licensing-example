package it;

import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.jira.testkit.client.util.TimeBombLicence;

import org.junit.Before;
import org.junit.Test;

public class MyPluginTest
{
	@Before
	public void setUp() {
		Backdoor testKit = new Backdoor(new TestKitLocalEnvironmentData());
		testKit.restoreBlankInstance(TimeBombLicence.LICENCE_FOR_TESTING);
		testKit.usersAndGroups().addUser("test-user");
		testKit.websudo().disable();
		testKit.subtask().enable();
	}

    @Test
    public void shouldDoSomething()
    {
		// Put your test logic here
    }
}
